{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    futils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, futils, flake-compat } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
      });
    in
    (eachDefaultSystem (system:
      let
        pkgs = nixpkgsFor.${system};
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            (poetry2nix.mkPoetryEnv {
              projectDir = self;

              overrides = poetry2nix.overrides.withDefaults (self: super: {
                cryptography = super.cryptography.overridePythonAttrs (old: {
                  CRYPTOGRAPHY_DONT_BUILD_RUST = 1;
                  propagatedBuildInputs = old.propagatedBuildInputs ++ [ super.setuptools-rust ];
                });
                enrich = super.enrich.overridePythonAttrs (old: {
                  propagatedBuildInputs = old.propagatedBuildInputs ++ [ super.toml ];
                });
                munch = super.munch.overridePythonAttrs (old: {
                  propagatedBuildInputs = old.propagatedBuildInputs ++ [ super.pbr ];
                });
                python-swiftclient = super.python-swiftclient.overridePythonAttrs (old: {
                  propagatedBuildInputs = old.propagatedBuildInputs ++ [ super.pbr ];
                });
                requestsexceptions = super.requestsexceptions.overridePythonAttrs (old: {
                  propagatedBuildInputs = old.propagatedBuildInputs ++ [ super.pbr ];
                });
                ruamel-yaml = python38Packages.ruamel_yaml.overridePythonAttrs (old: rec {
                  version = "0.17.2";

                  src = python3Packages.fetchPypi {
                    inherit (old) pname;
                    inherit version;
                    sha256 = "sha256-jx4VQhZoue3zDtAomfX4Gv+YCKQnGTV3b2GpmlaaE9o=";
                  };
                });
              });
            })
            git
            openssl
            poetry
            terraform_0_14
            vault
          ];

          shellHook = ''
            eval "$(cat config.sh)"
          '';
        };
      }
    ));
}
