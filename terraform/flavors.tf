data "openstack_compute_flavor_v2" "m1-tiny" {
  name = "m1.tiny"
}

data "openstack_compute_flavor_v2" "m1-small" {
  name = "m1.small"
}

data "openstack_compute_flavor_v2" "m1-medium" {
  name = "m1.medium"
}

data "openstack_compute_flavor_v2" "m1-large" {
  name = "m1.large"
}

data "openstack_compute_flavor_v2" "m1-xlarge" {
  name = "m1.xlarge"
}

data "openstack_compute_flavor_v2" "m1-2xlarge" {
  name = "m1.2xlarge"
}
