# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.0"
  hashes = [
    "h1:BZMEPucF+pbu9gsPk0G0BHx7YP04+tKdq2MrRDF1EDM=",
    "zh:2bbb3339f0643b5daa07480ef4397bd23a79963cc364cdfbb4e86354cb7725bc",
    "zh:3cd456047805bf639fbf2c761b1848880ea703a054f76db51852008b11008626",
    "zh:4f251b0eda5bb5e3dc26ea4400dba200018213654b69b4a5f96abee815b4f5ff",
    "zh:7011332745ea061e517fe1319bd6c75054a314155cb2c1199a5b01fe1889a7e2",
    "zh:738ed82858317ccc246691c8b85995bc125ac3b4143043219bd0437adc56c992",
    "zh:7dbe52fac7bb21227acd7529b487511c91f4107db9cc4414f50d04ffc3cab427",
    "zh:a3a9251fb15f93e4cfc1789800fc2d7414bbc18944ad4c5c98f466e6477c42bc",
    "zh:a543ec1a3a8c20635cf374110bd2f87c07374cf2c50617eee2c669b3ceeeaa9f",
    "zh:d9ab41d556a48bd7059f0810cf020500635bfc696c9fc3adab5ea8915c1d886b",
    "zh:d9e13427a7d011dbd654e591b0337e6074eef8c3b9bb11b2e39eaaf257044fd7",
    "zh:f7605bd1437752114baf601bdf6931debe6dc6bfe3006eb7e9bb9080931dca8a",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.40.0"
  constraints = "~> 1.40.0"
  hashes = [
    "h1:gBrsytNqUG1ZQPKys8KAvZkjesjimXb7vcrTmyFUTM0=",
    "zh:278a878a256ec5447e1e64b5d9a691e3a1f7d5c247e536500c97c5b996bc2531",
    "zh:5c7ae8cfe0831557c8c1988581f3fd0bdf182d15bcefbe645bb91564027e67d4",
    "zh:944d75fc1e3d54df4c47e5d34007927abf4fa79e2107b05d14f11b52970a6164",
    "zh:a50922d05185598a9264a25eff6f01ce7671c70a562a3ef93e9bb7a449e358b0",
    "zh:adb87ad3782f1f7a5eaeedbcffa0e5559d2372502f9af91781aa13c11cf4b47b",
    "zh:c0e4218259a37f16c10b4779009f0b0b5d467e4d347fc2aa3a212f1ee3a71d63",
    "zh:c2eb4f40cbd78238500a3a84ba995060bfc50f770bd13732ae50b73687f3dce6",
    "zh:ca8a38fe932972d0d7fdc51f84ae775648b7aff3c96b8ead085007e880ee987f",
    "zh:ce4f703719d646507d6006085dc1114954c75710226df43078169b2b01993537",
    "zh:e29542a492bbf55613d20b5f68ed4357cbc8bb09d61a1752d2976e5e1608879d",
    "zh:e68d47b85b9da089f8f7102c23545331c15a9e6ea99875926d2ebf6e38bf2073",
    "zh:fdb10cb345250d7c47e342def106bd10ef75493ef6edf15809e10e6367a0d9f6",
  ]
}
