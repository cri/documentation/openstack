data "openstack_dns_zone_v2" "iaas-training_cri_epita_fr" {
  name = "iaas-training.cri.epita.fr."
}

data "openstack_dns_zone_v2" "cri-training_iaas_epita_fr" {
  name = "cri-training.iaas.epita.fr."
}
