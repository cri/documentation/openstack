module "sg_egress-all" {
  source               = "git::https://gitlab.cri.epita.fr/cri/iac/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "All Egress traffic"
  description          = <<EOT
    Allow all Egress traffic. This security group is meant to be used with
    other ones.
  EOT
  delete_default_rules = true

  egress_rules_ipv4 = [{ remote_ip_prefix = "0.0.0.0/0" }]
  egress_rules_ipv6 = [{ remote_ip_prefix = "::/0" }]
}

module "sg_ingress-icmp" {
  source               = "git::https://gitlab.cri.epita.fr/cri/iac/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "All ICMP traffic"
  description          = <<EOT
    Allow all Egress traffic. This security group is meant to be used with
    other ones.
  EOT
  delete_default_rules = true

  ingress_rules_ipv4 = [{ protocol = "icmp" }]
  ingress_rules_ipv6 = [{ protocol = "ipv6-icmp" }]
}

module "sg_ingress-ssh" {
  source               = "git::https://gitlab.cri.epita.fr/cri/iac/terraform/modules/openstack-security-group.git?ref=0.3.1"
  name                 = "SSH Ingress only"
  description          = <<EOT
    Only allow SSH Ingress. This security group is meant to be used with other
    ones.
  EOT
  delete_default_rules = true

  ingress_rules = [{
    protocol       = "tcp"
    port_range_min = 22
    port_range_max = 22
  }]
}
