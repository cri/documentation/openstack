# dummy
resource "openstack_compute_keypair_v2" "dummy" {
  name       = "dummy"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKbwWBzjZJMrKHgBd6/rCfj2LS2+xYc/9piEPHjep6AE dummy"
}

# adrien
resource "openstack_compute_keypair_v2" "adrien" {
  name       = "adrien"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCkHw3+55/qWb9ycINOaAhKFiJjkvctP6Sw96upwVVlvFVOA5CCFGnpDWqB1aHCxzYQ/UKlceW7T08a6BDK9jVGlYhbPjFoQvlXqCdtRqj2ZYlEkhursD4rqCgPAM74RTGulHyuYIfjE+B8EJnXAgGkKspVoAjmHKAtnioJ5JSQJWKn584ttmfxGcU6z5HCyr0/yn8mNKd3KhZjovdT8IKxriWgz/u61viwVr+UvnrOk5EjrlWUGzZVxGLkpm6Y+fTCCCu03SGj8w9nxFfvAtZROOC/WknrBNoz2h79Kn6lex4w1EjcAZOyx7DC/2jehw1cWWHd0ZEcm05F18DsYn5061VzmoT4bhmYGG/riDxPzNklK7MfJ25Hso+iGGXPUbq+SeNCOx1mvGcHv0H9b+XmW4eV5vK6F63m2I+MHe/i4wfuvO2ggMDlpFZb8L4JDwYbCHvQ6UXnBHoBUVv0gBO2vNCCi35YyPKRuzpvi1AAe+N3ME8ErFWXjhR2n99F53j7qDUhVQLnQflgzERnMnq0M4mshKPYEwAz0ydFxYG+Zkae6wOwjzew2DjYvqyRD5pFTMSXvN6QRWLmXOUG3Uf8epd8JEJj0Z+TplWNkUDdPx5wXBJdtT6MueoPxLbswDyZq7axxdVg8Q4Zf6TbMKtCNfgU5LRKcVXYHYxtvNBW6Q== adrien@cri.epita.fr"
}

# chewie
resource "openstack_compute_keypair_v2" "chewie" {
  name       = "chewie"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQChM3SuJ4fNFgb6gIKPhjuz+PPW2KE2G2w+Q5GJ+1yrpuQN/tzNc90Fq45WD+PvjI6k2xqvkULFFpPk1Rsna1QRNlR6PIRPs1reOyh2nJOIpZlZPDpe7FItgaSHdsjVUopHsIR0sEWPbp6eIjk4UromN7jbhEW8DU6RdCG4PVdoRi3WgehWf3+Ut4mnYpN9cr7Mm6ZrtpHOIbLBwHi2ycF8C5J8BD0VFTKACnWulvvUacaQoUzhLC5lpIjE59jG9J7ysDyp/6m+bCX8zGuTUDcOdPn9+vSwV+IvJ6s8u4nmiE2UlkDpe0hOP2BDxE/okpwEWwNWhRuOhNJI+cHFeUFn chewie@armadillo"
}

# mareo
resource "openstack_compute_keypair_v2" "mareo" {
  name       = "mareo"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDRjWonANQ/xE+bAU6e0Wd2s97ONLuHP9EPxdeQTf48NdMOBq/Zyuej8xRd91tHjsF230wMkQemDkSWgEmM9w99yXVt3IOtRizchAQLKEq+3R0eU6gES/gFZ9VL6bNei0jvWAhqNDn7bb/k5FmS+Joy4nsINxmHPBzhJFlcGfENrpUl/lPfWOoldkEjNZ8Wzaxx+OIcvoxsITlOVLu5zD/sRhDS82R6Dr4xPnJxVUxHfmB+ypRTfjA/gBW3JLFxe/GvgpfNpX20OsZPlzyLedW/Km3v3kUFDM5ygAArIAi/LCGohYLF+qkofrj3IM+mxI98ysa8g6SA5jKpC/SA0mZbadUfQJRrFYJp0cJcweMqshqwYG1F4uxm0dv2XTMaoSTn+RixKhIYi9TZK6FWzSZf96tb+n17ZybSv+y+KB1Qa5eJxxaGdFbwO2XAXLtTlhSfPW96AKOSD+d+0N+lJLPOj8HpQiv7+Qq2tUmtNIbelfg7Fzeei9WcsAJvXiHlj5ZOKREsZwe8Z+7gy1XtS57yaq2ogx27vpEYpPqjpX75LSvwxuDBr/5/gEZfDhucLo0LNT9w3mxu6LZCuQB1hNER2IoIADEvizEHyEBguqPYWo7542WJn87nXpy9wwYo7R5Pmv0hVOXCl9NcuEIcZDcvB31cjLLOEf2C546umjMVvw== Marin Hannache (Mareo)"
}

# melchior
resource "openstack_compute_keypair_v2" "melchior" {
  name       = "melchior"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9x9hPa7a9DehZFZeofWNwLKl7vCcqakPhIqon3ULqSv0L2wvEfJMMNMVIE2XWK2zjfWVUgK0JJmpUoLMFk5jSfJd7FadcZdNRbsX8OftXMw9bb7KH0vXc4mz2JQ+PKOzOw93ThIkQaziqWQj33i4euAgg3e/HnwbxtK9JscyJfLo75lsxQj80qMwc7fZz7ljPNcYAOHI7Vw7sT5zZ6/a3FDsgKQSc75gj7onBXMCe8ZnlHhVwSZCmZRQoNJFycs7itV/bwTaBzv4KIl6HXO8KX+M+Hg4T04W9doiPFbM52l0TDFk6Un0uMl00FvBAL68wUQU3eK8wn5BoMlLXS8YRnKGvekGuAYHZSiy2ugv21Yp39vpIyJJ5bhgFTG5CAJHfLnmCJuG/hRLv5xxQHVWCnZuvP5CRQLJeoY3Oe6j1CcVT6H/KFWH1RVU9ax7VYg00KzO3rbVhjGjHtEVbu4scIYykhI2y0HM1aIspg4MfZBa21OAP5reJW7CrcbPfGNxBG/11bGL49bBKf58nl8yl4nMz3CvPdZmKNMdxxBpp+2QBQLBhKJ1kiz1b8i/1LP+P5fXgowXB6OuJweO8PlI/HIa+tcYAPFSoH8ll0SqBoUOQ0qv7vj9DqmaL+g/28VgVoM4UwTrmuJH5Yd3ZaGQKgon2zKI9l0ACNXukybodmw== Melchior de Roquefeuil (gitlab.cri.epita.fr)"
}

# risson
resource "openstack_compute_keypair_v2" "risson" {
  name       = "risson"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL0pnnKrvi9lrliSm+pf9HNAzs0GYLKiJk5AtSg4hhDq risson@yubikey"
}

# routmout
resource "openstack_compute_keypair_v2" "rootmout" {
  name       = "rootmout"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDKdMc2jTAvKNCRQYo+Azc7yKOXGwizJ0jh7zFCDouGqclAoHs8dYtXO//znVztBEpA9EHE/agIVqsZM5J5/u+LWnRgH5sCPIXbTVbJJZXWNKizVm3a48A3g1dAqr44b8e8+klPhm5ZfOnoGuWiJBe5C1pv9h3gUdC+V0Lkd4h2okJzu5ZJUPA7OO8iuS/cz+/lFrEyoFlDPnppBmr/RUp+xTknN6UvjkaD9xofg/aWoKwJnyX8eTeKtdMlvz8/4nUAoPm6FhMu5vp2C0sLO7AdROGjZT9FMFezy5tE2CrngYFMWP7wHfXK7pXyG1hBE1DZUQo3NdtdLbTgI34743xqrGUoTrsD6M3m7g15RCCbqq+9YBwbd62iLM3MDtPkASxlQ6YKS/Epn4z29BBQwcXDPjWEE40Vu7GQpvZ9EMXtzG2q0kZstWEyCv/bNuPrl/tduUEOyqKQOdA03YO1HhISoebEbvsCb8SRBcCOBn0qmdkRwPizYFSuhXmxwmZ+ipE= rootmout@rootmout-desk-cri"
}

locals {
  user_data_ssh_keys = <<EOF
#cloud-config
disable_root: false
ssh_authorized_keys:
  - ${openstack_compute_keypair_v2.adrien.public_key}
  - ${openstack_compute_keypair_v2.chewie.public_key}
  - ${openstack_compute_keypair_v2.mareo.public_key}
  - ${openstack_compute_keypair_v2.melchior.public_key}
  - ${openstack_compute_keypair_v2.risson.public_key}
  - ${openstack_compute_keypair_v2.rootmout.public_key}
EOF
}
