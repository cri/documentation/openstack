module "admin-svc-net" {
  source         = "git::https://gitlab.cri.epita.fr/cri/iac/terraform/modules/openstack-network.git?ref=0.3.1"
  name           = "admin-svc-net"
  admin_state_up = true

  subnets = [
    {
      cidr            = "192.168.99.0/24"
      router_id       = data.openstack_networking_router_v2.external-router-0.id
      gateway_ip      = "192.168.99.254"
      enable_dhcp     = true
      dns_nameservers = ["91.243.117.211"]
    },
  ]
}

resource "openstack_compute_instance_v2" "bastion-1" {
  name      = "bastion-1"
  image_id  = data.openstack_images_image_v2.ubuntu-20-04-20210201.id
  flavor_id = data.openstack_compute_flavor_v2.m1-small.id
  key_pair  = openstack_compute_keypair_v2.dummy.name
  user_data = local.user_data_ssh_keys
  security_groups = [
    module.sg_egress-all.security_group_name,
    module.sg_ingress-icmp.security_group_name,
    module.sg_ingress-ssh.security_group_name,
  ]

  network {
    uuid = module.admin-svc-net.network_id
  }
}

data "openstack_networking_floatingip_v2" "admin-svc" {
  address = "91.243.117.139"
}
resource "openstack_dns_recordset_v2" "admin-svc_iaas-training_cri_epita_fr" {
  zone_id = data.openstack_dns_zone_v2.iaas-training_cri_epita_fr.id
  name    = "admin-svc.iaas-training.cri.epita.fr."
  type    = "A"
  records = [data.openstack_networking_floatingip_v2.admin-svc.address]
}

resource "openstack_lb_loadbalancer_v2" "admin-svc" {
  name           = "admin-svc"
  vip_subnet_id  = module.admin-svc-net.subnets[0].id
  admin_state_up = true
}
resource "openstack_networking_floatingip_associate_v2" "admin-svc" {
  floating_ip = data.openstack_networking_floatingip_v2.admin-svc.address
  port_id     = openstack_lb_loadbalancer_v2.admin-svc.vip_port_id
}

resource "openstack_dns_recordset_v2" "bastion_iaas-training_cri_epita_fr" {
  zone_id = data.openstack_dns_zone_v2.iaas-training_cri_epita_fr.id
  name    = "bastion.iaas-training.cri.epita.fr."
  type    = "CNAME"
  records = [openstack_dns_recordset_v2.admin-svc_iaas-training_cri_epita_fr.name]
}
resource "openstack_lb_listener_v2" "admin-svc-ssh" {
  loadbalancer_id = openstack_lb_loadbalancer_v2.admin-svc.id
  protocol        = "TCP"
  protocol_port   = 22
}
resource "openstack_lb_pool_v2" "admin-svc-ssh" {
  listener_id = openstack_lb_listener_v2.admin-svc-ssh.id
  protocol    = "TCP"
  lb_method   = "LEAST_CONNECTIONS"
  persistence {
    type = "SOURCE_IP"
  }
}
resource "openstack_lb_members_v2" "admin-svc-ssh" {
  pool_id = openstack_lb_pool_v2.admin-svc-ssh.id

  member {
    address       = openstack_compute_instance_v2.bastion-1.network[0].fixed_ip_v4
    protocol_port = 22
  }
}
resource "openstack_lb_monitor_v2" "admin-svc-ssh" {
  pool_id     = openstack_lb_pool_v2.admin-svc-ssh.id
  type        = "TCP"
  delay       = 5
  timeout     = 5
  max_retries = 3
}
