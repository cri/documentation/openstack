terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.40.0"
    }
  }

  backend "swift" {
    container = "terraform"
    # this uses the clouds.yaml file
    cloud = "openstack"
  }
}

provider "openstack" {
  # this uses the clouds.yaml file
  cloud       = "openstack"
  use_octavia = true
}
